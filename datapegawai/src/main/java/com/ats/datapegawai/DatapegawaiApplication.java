package com.ats.datapegawai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatapegawaiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatapegawaiApplication.class, args);
	}

}
