package com.ats.datapegawai.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.datapegawai.model.PegawaiModel;
import com.ats.datapegawai.service.PegawaiService;

@Controller
public class PegawaiController {
	
	@Autowired
	private PegawaiService pegawaiservice;
	
	
	@RequestMapping("/daftar_pegawai")
	public String menuDaftar() {
		String html = "pegawai/daftar";
		return html;
	}
	@RequestMapping("/hasil_pegawai")
	public String menuHasil(HttpServletRequest request, Model model) {
		int noPegawai = Integer.valueOf(request.getParameter("daftar_no"));
		String namaPegawai = request.getParameter("daftar_nama");
		int gajiPegawai = Integer.valueOf(request.getParameter("daftar_gaji"));
		String statusPegawai = request.getParameter("daftar_status");
		
		PegawaiModel pegawaiModel = new PegawaiModel();
		pegawaiModel.setNo(noPegawai);
		pegawaiModel.setNamaPegawai(namaPegawai);
		pegawaiModel.setGajiPegawai(gajiPegawai);
		pegawaiModel.setStatus(statusPegawai);
		
		pegawaiservice.create(pegawaiModel);
		
		List<PegawaiModel>pegawaiModelList = new ArrayList<PegawaiModel>();
		pegawaiModelList = pegawaiservice.read();
		model.addAttribute("pegawaiModelList", pegawaiModelList);
		
		String html = "pegawai/hasil";
		return html;
		
	}

}
