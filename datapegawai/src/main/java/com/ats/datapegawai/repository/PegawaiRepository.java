package com.ats.datapegawai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.datapegawai.model.PegawaiModel;

public interface PegawaiRepository extends JpaRepository<PegawaiModel,Integer>{

}
