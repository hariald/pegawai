package com.ats.datapegawai.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.datapegawai.model.PegawaiModel;
import com.ats.datapegawai.repository.PegawaiRepository;

@Service
@Transactional
public class PegawaiService {
	
	@Autowired
	private PegawaiRepository pegawaiRepository;
	
	public void create(PegawaiModel pegawaiModel) {
		pegawaiRepository.save(pegawaiModel);
	}
	
	public List<PegawaiModel> read() {
		return pegawaiRepository.findAll();
	}
}
